export const types = {
  SET_ANSWERS: '[Quiz] Set Answers',
  SET_IS_QUIZ_COMPLETE: '[Quiz] Is Quiz Complete',
  RESET_QUIZ: '[Quiz] Reset Quiz',
}
