import { ethers } from 'ethers'

import quizAbi from '../erc20abi/quiz.json'

const ADDRESS_CONTRACT_QUIZ = '0x74F0B668Ea3053052DEAa5Eedd1815f579f0Ee03'

const getTokenInfo = async () => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const erc20 = new ethers.Contract(ADDRESS_CONTRACT_QUIZ, quizAbi, provider)
  const [tokenName, tokenSymbol, totalSupply, decimals] = await Promise.all([
    erc20.name(),
    erc20.symbol(),
    erc20.totalSupply(),
    erc20.decimals(),
  ])
  return { tokenName, tokenSymbol, totalSupply, decimals }
}

const getMyBalance = async () => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  await provider.send('eth_requestAccounts', [])
  const erc20 = new ethers.Contract(ADDRESS_CONTRACT_QUIZ, quizAbi, provider)
  const signer = await provider.getSigner()
  const signerAddress = await signer.getAddress()
  const balance = await erc20.balanceOf(signerAddress)

  return {
    address: signerAddress,
    balance: ethers.utils.formatEther(balance),
  }
}

const sendQuiz = async (title: string, answers: string[]) => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  await provider.send('eth_requestAccounts', [])
  const signer = await provider.getSigner()
  const erc20 = new ethers.Contract(ADDRESS_CONTRACT_QUIZ, quizAbi, signer)
  await erc20.submit(title, answers, {
    gasPrice: ethers.utils.parseUnits('100', 'gwei'),
    gasLimit: 1000000,
  })
}

export { getTokenInfo, getMyBalance, sendQuiz }
