export const initStateContractInfo = {
  tokenName: '',
  tokenSymbol: '',
  totalSupply: '',
  decimals: '',
}

export const initStateBalanceInfo = {
  address: '',
  balance: '',
}
