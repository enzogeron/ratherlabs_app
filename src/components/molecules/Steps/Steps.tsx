import { FC } from 'react'
import { Steps as StepsAnt } from 'antd'

import { StepsProps } from './interface'

const { Step } = StepsAnt

const Steps: FC<StepsProps> = ({ current, questions, initialCount, count }) => {
  const percent = Math.floor(100 / initialCount)

  return (
    <StepsAnt current={current} percent={percent * count}>
      {questions.map((question, index) => (
        <Step key={index} subTitle={`Left ${count}s`} />
      ))}
    </StepsAnt>
  )
}

export default Steps
