import { QuestionProps } from '../../organisms/Quiz/interface'

export interface StepsProps {
  current: number
  questions: QuestionProps[]
  count: number
  initialCount: number
}
