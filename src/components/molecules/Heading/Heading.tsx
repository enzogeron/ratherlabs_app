import { FC } from 'react'
import Image from 'next/image'

import { Title } from '../../atoms'
import { HeadingProps } from './interface'
import styles from './Heading.module.scss'

const Heading: FC<HeadingProps> = ({ title, image }) => {
  return (
    <div className={styles.heading}>
      <Title className={styles.title} as="h1">
        {title}
      </Title>

      <Image src={image} alt={title} width={50} height={50} objectFit="cover" />
    </div>
  )
}

export default Heading
