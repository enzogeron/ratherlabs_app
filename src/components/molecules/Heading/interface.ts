export interface HeadingProps {
  title: string
  image: string
}
