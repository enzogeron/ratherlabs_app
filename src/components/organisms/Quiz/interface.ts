export interface QuizProps {
  title: string
  image: string
  questions: QuestionProps[]
}

export interface QuestionProps {
  text: string
  image: string
  lifetimeSeconds: number
  options: OptionsProps[]
}

interface OptionsProps {
  text: string
}
