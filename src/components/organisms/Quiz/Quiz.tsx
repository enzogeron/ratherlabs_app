import { FC, useCallback, useEffect, useState } from 'react'
import Image from 'next/image'
import { Checkbox } from 'antd'

import { Container, Title } from '../../atoms'
import { Heading, Steps } from '../../molecules'
import { setAnswers, setIsQuizComplete } from '../../../actions/quizActions'
import { useAppDispatch } from '../../../hooks/useAppDispatch'
import useCountdown from '../../../hooks/useCountDown'
import { QuizProps } from './interface'
import styles from './Quiz.module.scss'

const Quiz: FC<QuizProps> = ({ title, image, questions }) => {
  const dispatch = useAppDispatch()
  const [current, setCurrent] = useState(0)
  const [value, setValue] = useState([])
  const [disabledCheckbox, setDisabledCheckbox] = useState(false)
  const currentContent = questions[current]
  const isNotLastQuestion = questions.length > current + 1

  const [count, { start, reset, setCount }] = useCountdown({
    seconds: currentContent?.lifetimeSeconds,
    interval: 1000,
    isIncrement: false,
  })

  // Set Current Question
  const handleSetCurrent = useCallback(() => {
    if (isNotLastQuestion) {
      setCurrent(current + 1)
      start()
    } else {
      setDisabledCheckbox(true)
      dispatch(setIsQuizComplete(true))
    }
  }, [current, dispatch, isNotLastQuestion, start])

  // Start CountDown
  useEffect(() => {
    start()
  }, [start])

  // If the time is over
  useEffect(() => {
    if (count === 0) {
      reset()

      if (!disabledCheckbox) {
        setValue([
          ...value,
          {
            question: currentContent?.text,
            answer: null,
          },
        ])
      }

      handleSetCurrent()
    }
  }, [
    count,
    reset,
    handleSetCurrent,
    value,
    currentContent?.text,
    disabledCheckbox,
  ])

  // Set new CountDown
  useEffect(() => {
    setCount(currentContent?.lifetimeSeconds)
  }, [current, currentContent?.lifetimeSeconds, setCount])

  const onChangeCheckbox = ({ target }) => {
    reset()

    setValue([
      ...value,
      {
        question: currentContent?.text,
        answer: target.value,
      },
    ])

    handleSetCurrent()
  }

  useEffect(() => {
    dispatch(setAnswers(value))
  }, [dispatch, value])

  return (
    <section className={styles.quiz}>
      <Container>
        <Heading title={title} image={image} />

        <Steps
          current={current}
          questions={questions}
          count={count}
          initialCount={currentContent?.lifetimeSeconds}
        />

        <div className={styles.content}>
          <div className={styles['question-container']}>
            <Title className={styles['title-question']} as="h2">
              {currentContent?.text}
            </Title>

            <div className={styles['questions-and-image']}>
              <div className={styles.questions}>
                {currentContent.options.map(({ text }, index) => (
                  <div
                    key={`${index}-${current}`}
                    className={styles['checkbox']}
                  >
                    <Checkbox
                      onChange={onChangeCheckbox}
                      value={index + 1}
                      disabled={disabledCheckbox}
                    >
                      {text}
                    </Checkbox>
                  </div>
                ))}
              </div>

              <Image
                src={currentContent?.image}
                alt={currentContent?.text}
                width={200}
                height={200}
                objectFit="cover"
              />
            </div>
          </div>
        </div>
      </Container>
    </section>
  )
}

export default Quiz
