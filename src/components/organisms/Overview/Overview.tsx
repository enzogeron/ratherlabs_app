import { FC } from 'react'
import { shallowEqual } from 'react-redux'
import { useRouter } from 'next/dist/client/router'
import { Button, Modal } from 'antd'

import { Title, P } from '../../atoms'
import { useAppSelector } from '../../../hooks/useAppSelector'
import styles from './Overview.module.scss'
import { sendQuiz } from '../../../services/ethers'

const Overview: FC = () => {
  const { isQuizComplete, answers } =
    useAppSelector((state) => state.quiz, shallowEqual) || []
  const router = useRouter()

  // Submit
  const handleSubmit = async () => {
    await sendQuiz(
      '1',
      answers.map((a) => a.answer || 0)
    )
    router.push('/thanks')
  }

  return (
    <Modal
      title="Overview"
      centered
      visible={isQuizComplete}
      footer={null}
      closeIcon={null}
      className={styles['modal-contact']}
    >
      <div className={styles.content}>
        <div className={styles['overview-content']}>
          {answers.map(({ question, answer }, index) => {
            return (
              <div key={index} className={styles.row}>
                <Title className={styles.question} as="h3" size="sm">
                  {question}:
                </Title>
                <P className={styles.answer} size="sm">
                  {answer ?? 'Empty'}
                </P>
              </div>
            )
          })}
        </div>

        <Button type="primary" block onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    </Modal>
  )
}

export default Overview
