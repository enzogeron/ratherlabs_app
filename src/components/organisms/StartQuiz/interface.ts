export interface StartQuizProps {
  title: string
  image: string
  start?: (param) => void
}
