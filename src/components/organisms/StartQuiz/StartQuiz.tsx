import { FC, useState, useEffect } from 'react'
import { useSwitchNetwork, useWeb3 } from '@3rdweb/hooks'
import { Button } from 'antd'

import { StartQuizProps } from './interface'
import { Container, P } from '../../atoms'
import Heading from '../../molecules/Heading/Heading'
import { getMyBalance, getTokenInfo } from '../../../services/ethers'
import {
  initStateBalanceInfo,
  initStateContractInfo,
} from '../../../utils/index'
import styles from './StartQuiz.module.scss'

const StartQuiz: FC<StartQuizProps> = ({ title, image, start }) => {
  const { address, chainId, connectWallet, disconnectWallet } = useWeb3()
  const { switchNetwork } = useSwitchNetwork()

  const [network, setNetwork] = useState(0)
  const [contractInfo, setContractInfo] = useState(initStateContractInfo)
  const [balanceInfo, setBalanceInfo] = useState(initStateBalanceInfo)

  const initState = () => {
    setContractInfo(initStateContractInfo)
    setBalanceInfo(initStateBalanceInfo)
  }

  const getDataContract = async () => {
    const token = await getTokenInfo()
    const balance = await getMyBalance()

    setContractInfo(token)
    setBalanceInfo(balance)
  }
  useEffect(() => {
    chainId === 3 ? getDataContract() : initState()
  }, [])

  useEffect(() => {
    setNetwork(chainId)
    chainId === 3 ? getDataContract() : initState()
  }, [chainId])

  const changeNetworkRopsten = () => {
    switchNetwork(3)
    setNetwork(3)
  }

  const ValidateNetwork = () => {
    return (
      <>
        {network !== 3 ? (
          <Button type="primary" onClick={() => changeNetworkRopsten()}>
            Change Network Ropsten
          </Button>
        ) : (
          <Button type="primary" onClick={() => start(true)}>
            Go Quiz!
          </Button>
        )}
      </>
    )
  }

  return (
    <section className={`${styles['start-quiz']}`}>
      <Container>
        <Heading title={title} image={image} />
        {address ? (
          <>
            <P>Name: {contractInfo?.tokenName}</P>
            <P>Symbol: {contractInfo?.tokenSymbol}</P>
            <P>Total supply: {String(contractInfo?.totalSupply)}</P>
            <P>Decimals: {contractInfo?.decimals}</P>
            <P>Address balance: {balanceInfo?.address}</P>
            <P>Balance: {balanceInfo?.balance}</P>
            <div className={`${styles.validate}`}>
              <ValidateNetwork />
            </div>
          </>
        ) : (
          <>
            <Button type="primary" onClick={() => connectWallet('injected')}>
              Connect Metamask
            </Button>
          </>
        )}
      </Container>
    </section>
  )
}

export default StartQuiz
