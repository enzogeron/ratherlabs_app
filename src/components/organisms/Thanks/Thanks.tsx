import { NextPage } from 'next'

import { P } from '../../atoms'
import Container from '../../atoms/Container/Container'
import styles from './Thanks.module.scss'

const Thanks: NextPage = () => {
  return (
    <section>
      <Container>
        <P className={`${styles.message}`}>
          Thank you for participating in the survey!
        </P>
      </Container>
    </section>
  )
}

export default Thanks
