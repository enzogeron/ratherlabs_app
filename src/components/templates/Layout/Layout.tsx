import { FC } from 'react'

const Layout: FC = ({ children }: any) => {
  return <main>{children}</main>
}

export default Layout
