export interface PProps {
  children: JSX.Element | JSX.Element[] | string | string[]
  size?: 'sm' | 'md' | 'lg'
  className?: string
}
