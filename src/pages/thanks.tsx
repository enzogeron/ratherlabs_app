import { NextPage } from 'next'

import { HeadSeo, Layout } from '../components/templates'
import { Thanks } from '../components/organisms'

const ThanksPage: NextPage = () => {
  return (
    <>
      <HeadSeo title="Thanks" />
      <Layout>
        <Thanks />
      </Layout>
    </>
  )
}

export default ThanksPage
