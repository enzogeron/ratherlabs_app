// import App from "next/app";
import type { AppProps /*, AppContext */ } from 'next/app'
import { ThirdwebProvider } from '@3rdweb/react'
import { Provider } from 'react-redux'

import { store } from '../store/store'
import 'antd/dist/antd.css'
import '../styles/globals.scss'

declare global {
  interface Window {
    ethereum: any
  }
}

function MyApp({ Component, pageProps }: AppProps) {
  const supportedChainIds = [1, 3]

  const connectors = {
    injected: {},
  }

  return (
    <ThirdwebProvider
      supportedChainIds={supportedChainIds}
      connectors={connectors}
    >
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </ThirdwebProvider>
  )
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext: AppContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);

//   return { ...appProps }
// }

export default MyApp
