import { useState } from 'react'
import { NextPage } from 'next'

import { Overview, Quiz } from '../components/organisms'
import { HeadSeo, Layout } from '../components/templates'
import survey from '../data/survey.json'
import StartQuiz from '../components/organisms/StartQuiz/StartQuiz'

const IndexPage: NextPage = () => {
  const [initQuiz, setInitQuiz] = useState(false)

  return (
    <>
      <HeadSeo title="Quiz" />

      <Layout>
        {!initQuiz ? (
          <>
            <StartQuiz
              title={survey.title}
              image={survey.image}
              start={setInitQuiz}
            />
          </>
        ) : (
          <>
            <Quiz
              title={survey.title}
              image={survey.image}
              questions={survey.questions}
            />
            <Overview />
          </>
        )}
      </Layout>
    </>
  )
}

export default IndexPage
