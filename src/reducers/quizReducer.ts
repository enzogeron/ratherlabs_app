import { types } from '../types/types'

const initState = {
  answers: [],
  isQuizComplete: false,
}

export const quizReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_ANSWERS:
      return {
        ...state,
        answers: action.payload,
      }

    case types.SET_IS_QUIZ_COMPLETE:
      return {
        ...state,
        isQuizComplete: action.payload,
      }

    case types.RESET_QUIZ:
      return {
        ...initState,
      }

    default:
      return state
  }
}
