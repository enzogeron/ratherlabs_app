/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { types } from '../types/types'

export const setAnswers = (answers) => {
  return async (dispatch: any) => {
    await dispatch({
      type: types.SET_ANSWERS,
      payload: answers,
    })
  }
}

export const resetQuiz = () => {
  return async (dispatch: any) => {
    await dispatch({
      type: types.RESET_QUIZ,
    })
  }
}

export const setIsQuizComplete = (isComplete: boolean) => {
  return async (dispatch: any) => {
    await dispatch({
      type: types.SET_IS_QUIZ_COMPLETE,
      payload: isComplete,
    })
  }
}
