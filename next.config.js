const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'localhost',
      'interactive-examples.mdn.mozilla.net',
      'filedn.com',
      '48tools.com',
    ],
  },
}

module.exports = nextConfig
